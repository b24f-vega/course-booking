const jwt = require("jsonwebtoken");
const secret = "coursebookingAPI"


// token creation

module.exports.createAccessToken = (user) =>{
    const data = {
        _id:user._id,
        email:user.email,
        isAdmin:user.isAdmin
    }

    return jwt.sign(data, secret,{});
}

// token verification

module.exports.verify = (request,response,next) =>{
    let token = request.headers.authorization;
    
    if(typeof token !== "undefined"){
        token = token.slice(7,token.length)
        //console.log(token);
        return jwt.verify(token,secret,(err,data) =>{
            if(err){
                return response.send({auth:"Failed"});
            }
            else{
                next();
            }
        });
    }
    else{
        return response.send({auth: "Failed"})
    }
}
// token decryption

module.exports.decode = (token) => {
    if(typeof token !== "undefined"){
        token = token.slice(7,token.length)
        return jwt.verify(token,secret,(err,data) =>{
            if(err){
                return null;
            }
            else{
                return jwt.decode(token,{complete:true}).payload;
            }
        });
    }else{
        return null;
    }

}