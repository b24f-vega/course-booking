const mongoose = require("mongoose");
const User = require("../Models/usersSchema.js");
const Course = require("../Models/coursesSchema.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js")
//Controllers 
//This controller will create or register a user on our database



module.exports.userRegistration = (request, response) => {
    const input = request.body;
    User.findOne({email:input.email})
    .then(result =>{
        if(result !== null){
            return response.send("The Email is already taken")
        }
        else{
            let newUser = new User({
                firstName : input.firstName,
                lastName  : input.lastName,
                email : input.email,
                password : bcrypt.hashSync(input.password,10),
                mobileNo : input.mobileNo
            });
            //save to database 
            newUser.save().then(save => {
                return response.send("You are now registered to our website");
            })
            .catch(error => response.send(error));
        }
    })
    .catch(error => response.send(error));
}
module.exports.userAuthentication = (request,response) => {
    let input = request.body;
    User.findOne({email:input.email}).then(result =>{
        if(result === null){
            return response.send("Email is not yet registered")
        }
        else{
            const isPasswordCorrect = bcrypt.compareSync(input.password,result.password)
            if(isPasswordCorrect){
                return response.send({auth:auth.createAccessToken(result)})
            }else{
                return response.send("Incorrect password")
            }
        }
    })
    .catch(error =>{
        return  response.send(error);
    })
}
module.exports.getProfile = (request,response) => {
    // let input = request.body;
    const userData = auth.decode(request.headers.authorization);
    // console.log(userData);
    return User.findById(userData._id).then(result =>{
        result.password = "";
    return response.send(result);
    })
//     User.findById({_id:input._id}).then(result =>{
//         if(result === null){
//             return response.send("User does not exist!");
//         }
//         else{
//             result.password = "";
//             return response.send(result);
//         }
//     })
//     .catch(error => {
//         return response.send(error);
//     })
// 
}
// Controller for user enrollment:

module.exports.enrollCourse = async (request, response) => {
    const userData = auth.decode(request.headers.authorization);
    const courseId = request.params.courseId;
    if(userData.isAdmin){
        return response.send("Admins are not allowed to enroll")
    }
    else{
            let isUserUpdated = await User.findById(userData._id)
            .then(result => {
            if(result === null){
                return false
            }else{
                if(courseId.length === 24){
                    result.enrollments.push({courseId: courseId});
                    return result.save()
                    .then(save => true)
                    .catch(error => false)
                }
                else{
                    return response.send("Course ID does not exist"),false;
                }
            }
        })
        
        let isCourseUpdated = await Course.findById(courseId).then(result => {
            if(result === null){
                return false;
            }else{
                result.enrollees.push({userId: userData._id});
                return result.save()
                .then(save => true)
                .catch(error => false);
            }
        })
        
        console.log(isCourseUpdated);
        console.log(isUserUpdated);
        
        if(isCourseUpdated && isUserUpdated){
            return response.send("The course is now enrolled!");
        }else{
            return response.send("There was an error during the enrollment. Please try again!");
        }
    }
    
    
};
    