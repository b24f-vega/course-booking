const express = require("express");
const router = express.Router();
const userController = require("../Controllers/userController.js");
const auth = require("../auth.js");

//this route is responsible for registration of the user

router.post("/register", userController.userRegistration);

router.post("/login", userController.userAuthentication);

router.get("/details",auth.verify, userController.getProfile);

router.post("/enroll/:courseId", auth.verify, userController.enrollCourse);


module.exports = router;