const express = require("express");
const router = express.Router();
const courseController = require("../Controllers/courseController.js");
const auth = require("../auth.js");

//route for creating a course 

 router.post("/",auth.verify,courseController.addCourse);

//Route for retrieving all courses
router.get("/all", auth.verify, courseController.allCourses);

//Route for retrieving all active courses
router.get("/allActive", courseController.allActiveCourses);


router.get("/allInactive", auth.verify, courseController.allInactiveCourses);


//[Routes with Params]
//Route for retrieving detail/s of specific course
router.get("/:courseId", courseController.courseDetails);


router.put("/update/:courseId", auth.verify, courseController.updateCourse);

router.patch("/archive/:courseId", auth.verify, courseController.updateCourse);




 module.exports = router;