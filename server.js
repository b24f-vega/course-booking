const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./Routes/userRoutes.js");
const courseRoutes = require("./Routes/courseRoutes.js");
const port = 3001;

const app = express();

//middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

//connection string
mongoose.set('strictQuery',true);
mongoose.connect("mongodb+srv://admin:admin@batch245-vega.ien0lsm.mongodb.net/batch_Course_API_Vega?retryWrites=true&w=majority",{
    useNewUrlParser:true,
    useUnifiedTopology:true
})

let db = mongoose.connection;

// for error handling
db.on("error",console.error.bind(console,"Connection Error!"))

// for validation
db.once("open",()=>{console.log("We are connected to the cloud!")})

// userrouting
app.use("/user", userRoutes);

//courserouting
app.use("/course", courseRoutes)



app.listen(port,() => {
    console.log(`Server is running at port :${port}`)
})
