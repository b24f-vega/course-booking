const mongoose = require("mongoose");

let usersSchema = new mongoose.Schema({
    firstName:{
        type: String,
        required:[true,"First Name of the user is required"]
    },
    lastName:{
        type: String,
        required:[true,"Last Name of the user is required"]
    },
    email:{
        type: String,
        required:[true,"Email of the user is required"]
    },
    password:{
        type: String,
        required:[true,"Password of the user is required"]
    },
    isAdmin:{
        type:Boolean,
        default:false
    },
    mobileNo:{
        type:Number,
        required:[true,"Mobile Number of the user is required"]
    },
    enrollments:[{
            courseId:{
                type: String,
                    required:[true,"Course ID of the user is required"]
                },
            enrolledOn:{
                type:Date,
                default:new Date()
            },
            status:{
                type:String,
                default:"Enrolled"
            }
        }

    ]

});
module.exports = mongoose.model("User",usersSchema);