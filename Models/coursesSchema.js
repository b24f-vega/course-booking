const mongoose = require("mongoose");

let coursesSChema = new mongoose.Schema({
    name:{
        type: String,
        require: [true,"Name of the course is required!"]
    },
    description:{
        type: String,
        require: [true,"Description of the course is required!"]
    },
    price:{
        type: Number,
        require: [true,"Price of the course is required!"]
    },
    isActive:{
        type: Boolean,
        default:true
    },
    createdOn:{
        type:Date,
        default:new Date()
    },
    enrollees:[
        {
            userId:{
                type: String,
                require: [true,"UserID is required!"]
            },
            enrolledOn:{
                type:Date,
                default:new Date()
            }
        }
    ]
})

module.exports = mongoose.model("Course",coursesSChema);